// Jeu des mots : groupe MARC TRISTAN EMMA THEO JULIEN
const RANDOM_QUOTE_API_URL = "http://api.quotable.io/random";

const quoteDisplayElement = document.getElementById("quoteDisplay")
const quoteTexteElement = document.getElementById("quoteTexte")
const quoteInputElement = document.getElementById("quoteInput")
const timerElement = document.getElementById("timer")
const btnNextElement = document.getElementById("btnNext")

let myTime
let NombreTouches = 0
let TouchesCorrect
let TouchesCorrect2
let TouchesMinutes

let correct = true;

document.getElementById("quoteInput").onkeydown = function(e){

    if( !(e.key === "Shift" || e.key === "Backspace"))
    NombreTouches ++;
    //console.log(NombreTouches)
}

quoteInputElement.addEventListener('input', () => {

    TouchesCorrect = 0
    TouchesCorrect2 = 0
    const arrayQuote = quoteDisplayElement.querySelectorAll('span')
    const arrayValue = quoteInputElement.value.split('')
    arrayQuote.forEach((characterSpan, index) => {
        const character = arrayValue[index]
        if (character == null) {
            characterSpan.classList.remove('correct')
            characterSpan.classList.remove('incorrect')
            correct = false;
        }else if (character === characterSpan.innerText) {
            TouchesCorrect ++
            characterSpan.classList.add('correct')
            characterSpan.classList.remove('incorrect')
            correct = true;
        } else {
            TouchesCorrect2 ++
            characterSpan.classList.remove('correct')
            characterSpan.classList.add('incorrect')
            correct = false;
        }

    })

    if(correct) {
        showTime();
        setTimeout(renderNewQuote,5000);

        //renderNewQuote()
    }
})



// Fonction timer
function showTime(){

    quoteDisplayElement.innerText = "Temps écoulé: " + myTime + "s.\n" +
    "Nombres de touches pressés = " + NombreTouches + "\n" +
    "Touches justes pressés = " + TouchesCorrect + "\n" +
    "Touches fausses pressés = " + (NombreTouches - TouchesCorrect) + "\n" +
    "Touches par minutes = " + (NombreTouches / (myTime/60)) + "\n" +
    "% réussi = " + ((TouchesCorrect * 100 ) / NombreTouches).toFixed(2) + " %"
    quoteTexteElement.innerText = ""
    quoteInputElement.style.display = "none"
    timerElement.style.visibility = "hidden"
    NombreTouches = 0
    TouchesCorrect = 0
    TouchesCorrect2 = 0

}



// fonction récupération phrases
function getRandomQuote() {
    return fetch(RANDOM_QUOTE_API_URL)
        .then(response => response.json())
        .then(data => data);
}




 // Fonction de rendu des Phrases
async function renderNewQuote() {
    const quote = await getRandomQuote();
    quoteDisplayElement.innerText = ''
    quoteTexteElement.innerText = quote.author;
    quote.content.split('').forEach(character => {
        const characterSpan = document.createElement('span')
        characterSpan.innerText = character
        quoteDisplayElement.appendChild(characterSpan)
    })
    quoteInputElement.value = null;
    quoteInputElement.style.display = "block"
    timerElement.style.visibility = "visible"
    startTimer()
}

let startTime
function startTimer(){
    timerElement.innerText = 0
    startTime = new Date()
    setInterval(() => {
        timerElement.innerText = getTimerTime()
        myTime = getTimerTime()
    }, 1000);
}

function getTimerTime(){
    return Math.floor((new Date() - startTime) / 1000)
}

function nextQuote(){
    renderNewQuote();
}

renderNewQuote()
